
import {getHeroeById} from "./bases/08-imp-exp"
import heroes from "./bases/data/heroes";


// const promesa = new Promise((resolve, reject) => {

//     setTimeout ( () => {
        //Tarea 
        // importar el 
//         const heroe = getHeroeById(2);
        
        // console.log(heroe);
    
//     resolve( heroe );
    // reject ( "No se pudo encontrar el héroe" );

//     }, 2000 )

// });

// promesa.then( (heroe) => {
//     console.log("heroe", heroe)
// })

// .catch ( err => console.log( err ));


const getHeroeByIdAsync = ( id ) => {

return new Promise((resolve, reject) => {

    setTimeout ( () => {
        
    const p1 = getHeroeById( id );
    
    if(p1){
        
        resolve( p1 );
    
    } else{

        reject ("No se pudo encontrar el héroe" );
    }
    

    }, 2000 )

});

} 
getHeroeByIdAsync(3)
.then ( console.log)
.catch ( err => console.warn(err) );
