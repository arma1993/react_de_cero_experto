// Desestructuración
// Asignación Desestrocturante 

// importatante saberlo 


// const { nombre, edad, clave, } = persona;

// console.log( persona );
// console.log(edad);
// console.log(clave);

const persona = {

    nombre: "tony",
    edad: 45,
    clave: "ironman",
    // rango: "Soldado",
    // si existe la  propiedad en el objeto no hace falta 
    // definir demtro de las llaves 
};
const usarContexto = ( { nombre, edad, clave, rango = "capitan"} ) => {
    
    // console.log ( nombre,edad,clave, rango, );

    return{
        nombreClave: clave,
        anios: edad,
        latlng:{
            lat: 1253666,
            lng: -12.2566,
        }
    }
};

const {nombreClave, anios, latlng:{lat, lng,} } = usarContexto(persona);


console.log(nombreClave, anios,);
console.log(lat, lng);
// console.log(useContext)

